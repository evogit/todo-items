Desarrollar una aplicación que permitirá a los usuarios crear y gestionar varias tareas.
La aplicación debe almacenar los  siguientes datos en una estructura de base de datos
correctamente normalizada:

1. Nombre de la tarea.
2. Prioridad de la tarea.
3. Fecha de vencimiento.

Las tareas serán de propiedad/asignadas a un único usuario (el creador de la tarea) y deben alertar
al usuario que alguna tarea se va a vencer a través de la interfaz gráfica al iniciar sesión.

Implementar una vista única con una interfaz tipo "modal" para añadir los elementos pendientes y
una interfaz de lista que permite al usuario gestionar (eliminar / editar) las tareas pendientes.

La interfaz puede ser implementada utilizando cualquier libreria de javascript (Angular, React, Vuejs, ..etc)
y hay puntos extras por hacer una aplicación tipo [SPA](https://en.wikipedia.org/wiki/Single-page_application)
en el cliente.

El sistema debe requerir autenticación/autorización y sólo hará una lista y/o permitir la edición de las tareas
pendientes al usuario actualmente conectado.
La autenticación debe hacerse a través de combinación de correo electrónico/contraseña,

Ademas se debe desarrollar una interfaz (backend) simple para añadir usuarios a la aplicación, la creación de usuarios
debe ser proporcionada mediante correo electrónico, contraseña, confirmación de contraseña.

Se espera una completa funcionalidad de las características mencionadas anteriormente,
así como la interfaz del usuario (frontend).

Sólo los navegadores más recientes deben ser soportados.

La elección de que tecnología usar se deja al desarrollador, pero se requieren todos los archivos del backend y el frontend.
Hay puntos extras por realizar el backend en:

1. NodeJs (express)
2. PHP (Laravel, Symfony)

Se tendrán en cuenta los siguientes criterios a la hora de revisar la aplicación:

1. Comprensión de los requisitos.
2. Calidad de código (limpio, comentarios).
3. El uso de las tecnologías disponibles.
4. Validación de datos
5. Las estructuras de datos.
6. Que funcione correctamente. 

Por favor, proporcione un vínculo al repositorio "github/bitbucket/gitlab" con el código de la aplicación.
El repositorio debe incluir instrucciones para configurar y poner en marcha la aplicación

